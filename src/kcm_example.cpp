// SPDX-FileCopyrightText: 2024 Akseli Lahtinen <akselmo@akselmo.dev>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#include "kcm_example.h"

#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(ExampleModule, "kcm_example.json")

ExampleModule::ExampleModule(QObject *parent, const KPluginMetaData &data)
    : KQuickConfigModule(parent, data)
{
    setButtons(Help | Apply | Default);
}

#include "kcm_example.moc"
